import Vue from 'vue'
import VueRouter from 'vue-router'
import Form from '../views/Form.vue'
import RatedJoint from '../views/RatedJoint.vue'
import RatedJointSimple from '../views/RatedJointSimple.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Form',
        component: Form
    },
    {
        path: '/joint/:id',
        name: 'Joint',
        component: RatedJoint
    },
    {
        path: '/joint',
        name: 'JointSimple',
        component: RatedJointSimple
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router
